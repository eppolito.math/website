import init, { Universe, Cell } from "./wasm_game_of_life.js";

const wasm = await init();

const CELL_SIZE = 10;
const GRID_COLOR = "#CCCCCC";
const DEAD_COLOR = "#FFFFFF";
const ALIVE_COLOR = "#000000";

let width = 32;
let height = 32;

let universe = Universe.random(width, height);

const resetUniverse = () => {
    pause();
    universe = Universe.empty(width, height);
    drawCells();
};

const randomUniverse = () => {
    pause();
    universe = Universe.random(width, height);
    drawCells();
};

const canvas = document.getElementById("game-of-life-canvas");
canvas.height = (CELL_SIZE + 1) * height + 1;
canvas.width = (CELL_SIZE + 1) * width + 1;

const ctx = canvas.getContext('2d');

const drawGrid = () => {
    ctx.beginPath();
    ctx.strokeStyle = GRID_COLOR;

    for (let i = 0; i <= width; i++) {
        ctx.moveTo((CELL_SIZE + 1) * i + 1, 0);
        ctx.lineTo((CELL_SIZE + 1) * i + 1, (CELL_SIZE + 1) * height + 1)
    }

    for (let i = 0; i <= height; i++) {
        ctx.moveTo(0, (CELL_SIZE + 1) * i + 1);
        ctx.lineTo((CELL_SIZE + 1) * width + 1, (CELL_SIZE + 1) * i + 1);
    }

    ctx.stroke();
};

const getIndex = (row, column) => {
    return row * width + column;
};

const drawCells = () => {
    const cellsPtr = universe.cells();
    const cells = new Uint8Array(wasm.memory.buffer, cellsPtr, width * height);

    ctx.beginPath();

    for (let row = 0; row < height; row++) {
        for (let col = 0; col < width; col++) {
            const index = getIndex(row, col);

            ctx.fillStyle = cells[index] === Cell.Dead
                ? DEAD_COLOR
                : ALIVE_COLOR;

            ctx.fillRect(
                col * (CELL_SIZE + 1) + 1,
                row * (CELL_SIZE + 1) + 1,
                CELL_SIZE,
                CELL_SIZE
            );
        }
    }

    ctx.stroke();
};

const step = () => {
    universe.tick();
    drawCells();
};

/////////////////////////////////////////////////////////////////////////////////
// animation setup
let animationId = null;
let animationRate = 2;
const animationRateMaximum = 60; // frames per second

const decreaseAnimationRate = () => {
    animationRate = Math.max(1, animationRate - 1);
    // console.log(`animation rate decreased to ${animationRate}`);
};

const increaseAnimationRate = () => {
    animationRate = Math.min(animationRateMaximum, animationRate + 1);
    // console.log(`animation rate increased to ${animationRate}`);
};

const animate = () => {
    step();
    animationId = setTimeout(animate, 1000 / animationRate);
};

// const play = () => {
//     animate();
// };
const pause = () => {
    clearInterval(animationId);
    animationId = null;
};

const playOrPause = () => {
    if ( animationId == null ) {
        animate();
        // console.log("animation started");
    } else {
        pause();
        // console.log("animation paused");
    }
};

const stepAnimation = () => {
    if ( animationId != null ) { pause() };
    requestAnimationFrame(step);
    // console.log("animation step requested");
};

// TODO: find a way to add at cursor point

// control the thing
document.addEventListener(
    "keypress",
    function (event) {
        switch ( event.key ) {
        case "+": increaseAnimationRate(); break;
        case "-": decreaseAnimationRate(); break;
        case " ": stepAnimation(); break;
        case "Enter": playOrPause(); break;
        case "e": resetUniverse(); break;
        case "E": resetUniverse(); break;
        case "r": randomUniverse(); break;
        case "R": randomUniverse(); break;
        case "x": universe.insert_explosion(16, 16); drawCells(); break;
        case "c": universe.insert_crawler(16, 16); drawCells(); break;
        default:
            // console.log(`event ignored: key pressed was "${event.key}"`);
        }
    }
);

// add interactivity to the canvas
canvas.addEventListener(
    "click",
    event => {
        const boundingRect = canvas.getBoundingClientRect();

        const scaleX = canvas.width / boundingRect.width;
        const scaleY = canvas.height / boundingRect.height;

        const canvasLeft = scaleX * (event.clientX - boundingRect.left);
        const canvasTop  = scaleY * (event.clientY - boundingRect.top);

        const row = Math.min(Math.floor(canvasTop / (CELL_SIZE + 1)), height - 1);
        const col = Math.min(Math.floor(canvasLeft / (CELL_SIZE + 1)), width - 1);

        universe.toggle_cell(row, col);

        drawCells();
    }
);

// initial animation
requestAnimationFrame(function () { drawGrid(); drawCells(); });
