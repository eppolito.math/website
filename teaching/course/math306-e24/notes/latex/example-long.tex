% This file written by Chris Eppolito in 2023.
% I explain what each line does on my website, which you should consider consulting if you have any questions.
% See https://eppo-math.codeberg.page/
%
\documentclass[12pt]{amsart}
\usepackage[margin=1in]{geometry}

\usepackage{amsthm, amssymb, mathtools}
\newtheorem{theorem}{Theorem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\theoremstyle{definition}
\newtheorem*{definition}{Definition}
\newtheorem{example}[theorem]{Example}
\theoremstyle{remark}
\newtheorem{remark}{Remark}
\newtheorem{problem}{Problem}

\usepackage[capitalise,nameinlink]{cleveref}
\crefname{theorem}{Theorem}{Theorems}
\crefname{proposition}{Proposition}{Propositions}
\crefname{lemma}{Lemma}{Lemmas}
\crefname{corollary}{Corollary}{Corollaries}
\crefname{example}{Example}{Examples}
\crefname{definition}{Definition}{Definitions}
\crefname{remark}{Remark}{Remarks}
\crefname{problem}{Problem}{Problems}

\usepackage[citestyle=alphabetic,style=alphabetic,maxbibnames=9]{biblatex}
\renewbibmacro{in:}{\ifentrytype{article}{}{\printtext{\bibstring{in}\intitlepunct}}}
\DeclareFieldFormat{pages}{#1}
\DeclareFieldFormat{doi}{}
\DeclareFieldFormat{isbn}{}
\DeclareFieldFormat{issn}{}
\DeclareFieldFormat{url}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{\LaTeX{} Tutorial}
\author{Chris Eppolito}
\date{Created 2023-01-18}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\addbibresource{latex-example-references.bib}

\begin{document}
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{In-line and Display Mathematics}

The best equation ever discovered is Euler's Identity, \( e^{i \pi} + 1 = 0 \).
This fundamental identity was discovered by Leonhard Euler.
He wrote about it in the book \textit{Introductio in analysin infinitorum}, which appeared in 1748.

Since then, many mathematicians have been inspired by this equation to find generalizations.
Euler already knew that this was not as general as it could be; indeed, this is a special case of the general formula \( e^{i \theta} = \cos(\theta) + i \sin(\theta) \).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{De Moivre's Formula}

An interesting theorem related to Euler's identity is De Moivre's Formula.
\begin{theorem}[De Moivre's Formula]
  For all real numbers \( \theta \) and all positive integers \( n \) we have
  \[
    (\cos(\theta) + i\sin(\theta))^n
    = \cos(n\theta) + i\sin(n\theta)
    .
  \]
\end{theorem}
In fact, this theorem has an easy proof using Euler's Identity.
\begin{proof}
  Let \( \theta \in \mathbb{R} \) and \( n \in \mathbb{Z} \) be arbitrary.
  Applying the generalization of Euler's identity and usual laws of exponents, we obtain the following.
  \begin{align*}
    (\cos(\theta) + i\sin(\theta))^n
    &= (e^{i\theta})^n \\
    &= e^{i (n\theta)} \\
    &= \cos(n\theta) + i\sin(n\theta)
      .
  \end{align*}
  Hence the original statement holds.
\end{proof}

Note that the proof we gave applies more generally.
Indeed, the same computation proves the following more general identity.
\begin{proposition}
  For all real numbers \( \theta, r \) we have
  \[
    (\cos(\theta) + i\sin(\theta))^r
    = \cos(r\theta) + i\sin(r\theta)
    .
  \]
\end{proposition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
One generalization from abstract algebra---from field theory in particular---is that the \( n^{\textrm{th}} \) roots of unity sum to 0 for all positive integers \( n \).
An \emph{\( n^{\textrm{th}} \) root of unity} is a solution to the equation \( x^n - 1 = 0 \).
Thus, the second roots of unity are \( 1 \) and \( -1 \), as these are precisely the solutions of \( x^2 - 1 = 0 \).
For general \( n \), there are exactly \( n \) roots of unity; moreover, these have the formula \( \mu_n^k = e^{2\pi i \frac{k}{n}} \) where \( 0 \leq k \leq n-1 \).
In particular, this generalization asserts that the following equation holds:
\[
  \sum_{k=0}^{n-1} \mu_n^k
  = \sum_{k=0}^{n-1} e^{2\pi i \frac{k}{n}}
  = 0
  .
\]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Geometry and Roots of Unity}

There is some interesting geometry concerning roots of unity.
\begin{theorem}
  \label{result:n-gon-roots}
  If \( n \geq 3 \), then in the complex plane, the \( n^{\text{th}} \) roots of unity form the vertices of a regular \( n \)-gon.
  Moreover, this \( n \)-gon is inscribed entirely in the unit circle in the complex plane.
\end{theorem}
In fact, \cref{result:n-gon-roots} is another simple consequence of previous results.
By combining Euler's Formula and De Moivre's Formula, we have
\begin{equation}
  \label{eqn:roots-de-moivre}
  \mu_n^k
  = (e^{i\tfrac{2\pi}{n}})^k
  = (\cos(\tfrac{2\pi}{n}) + i \sin(\tfrac{2\pi}{n}))^k
  = \cos(k \tfrac{2\pi}{n}) + i \sin(k \tfrac{2\pi}{n})
  .
\end{equation}
Moreover, by elementary trigonometry, we know
\begin{equation}
  \label{eqn:trig-unit-circle}
  \cos^2(\theta) + \sin^2(\theta)
  = 1
  .
\end{equation}
By combining \cref{eqn:roots-de-moivre,eqn:trig-unit-circle}, we obtain
\[
  |\mu_n^k|
  = \sqrt{\cos^2(k \tfrac{2\pi}{n}) + \sin^2(k \tfrac{2\pi}{n})}
  = \sqrt{1}
  = 1
  .
\]
This justifies the claim that the corners lay entirely on the unit circle.
The rest of \cref{result:n-gon-roots} can be justified by proving \( |\mu_n^{k+1} - \mu_n^k| = |\mu_n^2 - \mu_n| \) for all \( 0 \leq k \leq n-1 \).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{More Reading}

There are lots of places we could read more about Euler's identity and other classical formulas in mathematics.
Some readers might find the website \cite{websiteWithAnOpinion} interesting, where the author gives some of their favourite equations.
One of the better resources on Euler's accomplishments is \cite{expensiveBook}.
Finally, \cite{bookOfNumbers} is an excellent textbook for mathy folks.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\printbibliography
\end{document}
