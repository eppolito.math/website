% This file written by Chris Eppolito in 2023.
% I wrote a LaTeX tutorial on my website, which you should consider consulting if you have any questions.
% See https://eppo-math.codeberg.page/
%
\documentclass[12pt]{exam}
\usepackage[margin=1in]{geometry}
\usepackage{amsthm, amssymb, mathtools}
\usepackage[dvipsnames]{xcolor} % this is safe to remove
\newtheorem{theorem}{Theorem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\theoremstyle{definition}
\newtheorem*{definition}{Definition}
\newtheorem{example}[theorem]{Example}
\theoremstyle{remark}
\newtheorem{remark}{Remark}
\newtheorem{problem}{Problem}
\usepackage[capitalise,nameinlink]{cleveref}
\crefname{theorem}{Theorem}{Theorems}
\crefname{proposition}{Proposition}{Propositions}
\crefname{lemma}{Lemma}{Lemmas}
\crefname{corollary}{Corollary}{Corollaries}
\crefname{example}{Example}{Examples}
\crefname{definition}{Definition}{Definitions}
\crefname{remark}{Remark}{Remarks}
\crefname{problem}{Problem}{Problems}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% IF YOU NEED A BIBLIOGRAPHY, PUT THAT SETUP HERE

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\newcommand{\myRGB}[3]{\textcolor{red}{#1}\textcolor{green}{#2}\textcolor{blue}{#3}} % REMOVE THIS!
\newcommand{\myName}{\myRGB{My }{Name }{Here}}   % MODIFY THIS!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\myAssignment}{Group Work Problems}
\header{\myName}{\Large{\myAssignment}}{\small{\today}} % NO TOUCHY!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INSTRUCTIONS: Solve the following problems as a group.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{problem}
  Let \( S \) be a set and let \( \mathbb{P}(S) \) denote its powerset.
  For each of the operations \( \cup \) (union), \( \cap \) (intersection), \( \setminus \) (set difference), and \( \triangle \) (symmetric difference).
  For each of these operations, \( \ast \), prove or disprove: \( (\mathbb{P}(S), \ast) \) is a group.
\end{problem}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{problem}[Optional]
  Let \( A \) be a set with a binary operaton \( \cdot \).
  Define an operation \( \ast \) on \( A \) by \( x \ast y = y \cdot x \).
  \begin{enumerate}
  \item %
    Prove that \( (A, \cdot) \) is a group if and only if \( (A, \ast) \) is a group.
  \item %
    Prove that if \( \cdot \) is associative and there is an \( e \in A \) satisfying
    \begin{enumerate}
    \item %
      \( ex = x \) for all \( x \in A \), and
    \item %
      for all \( x \in A \) there is a \( y \in A \) for which \( xy = e \),
    \end{enumerate}
    then \( (A, \cdot) \) is a group.
  \item %
    Prove that if \( \cdot \) is associative and there is an \( e \in A \) satisfying
    \begin{enumerate}
    \item %
      \( xe = x \) for all \( x \in A \), and
    \item %
      for all \( x \in A \) there is a \( y \in A \) for which \( xy = e \),
    \end{enumerate}
    then \( (A, \cdot) \) is a group.
  \item %
    Use the previous parts to conclude that the axioms for groups can be weakened to assuming any combination of axioms of left/right identity and inverse axioms.
    For example, we could instead assume the right identity property (\(\forall x \in A [ xe = x ] \)) and the left inverse property (\( \forall x \in A \exists y \in A [ yx = e ] \)).
  \end{enumerate}
\end{problem}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{problem}
  Let \( S \) be a set and \( (G, \cdot) \) a group.
  Suppose \( f \colon S \to G \) is a bijection with inverse function \( g \colon G \to S \).
  Prove that the operation defined on \( S \) by \( x \ast y = g(f(x) \cdot f(y)) \) makes \( (S, \ast) \) into a group.
\end{problem}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{problem}[Optional]
  \newcommand{\abs}{\operatorname{abs}}
  \newcommand{\sgn}{\operatorname{sgn}}
  \newcommand{\N}{\mathbb{N}_0}
  \newcommand{\Z}{\mathbb{Z}}
  We define an operation on \( \N \) via
  \begin{align*}
    (2x + a) \ast (2y + b)
    & = 2(\abs((-1)^{1 + a}(x + a) + (-1)^{1 + b}(y + b)) \\
    & \phantom{======} - (\sgn((-1)^{1 + a}(x + a) + (-1)^{1 + b}(y + b)) + 1) / 2) \\
    & \phantom{===} + (\sgn((-1)^{1 + a}(x + a) + (-1)^{1 + b}(y + b)) + 1) / 2
    .
  \end{align*}
  This operation can be shown to be a group operation in a number of steps.
  First, define functions
  \begin{align*}
    f &\colon \N \to \Z, & f(2q + r) &= (-1)^{1 + r} (q + r) \text{ for } 0 \leq r \leq 1 \text{ and } q, r \in \N \\
    g &\colon \Z \to \N, & g(n)      &= 2(\abs(n) - (\sgn(n) + 1) / 2) + (\sgn(n) + 1) / 2 \text{ if } n \neq 0 \text{ and } g(0) = 0,
  \end{align*}
  where \( \abs \) is the absolute value function and \( \sgn \) is the signum function defined by
  \[
  \sgn(x) =
  \begin{cases}
    1  &\text{ if } x > 0, \\
    0  &\text{ if } x = 0,\text{ and } \\
    -1 &\text{ if } x < 0.
  \end{cases}
  \]
  \begin{enumerate}
  \item %
    The operation \( m \ast n = g(f(m) + f(n)) \).
  \item %
    If \( n \neq 0 \), then \( (-1)^(1 + (\sgn(n) + 1) / 2) = \sgn(n) \).
  \item %
    If \( r \in \{0, 1\} \), then \( ((-1)^{1 + r} + 1) / 2 = r \).
  \item %
    The functions \( f \) and \( g \) are inverse functions.
  \item %
    Prove that \( (\N, \ast) \) is a group.
  \end{enumerate}
\end{problem}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{problem}
  \newcommand{\myRing}{\mathbb{R}}
  \newcommand{\mat}{\operatorname{GL}_{2}(\myRing)}
  You've proved in another homework problem that \( \mat \) is a group under matrix multiplication \( \cdot \).
  Prove or disprove: \( (\mat, \cdot) \) is abelian.
\end{problem}

%% RESOURCES FOR LaTeX
% https://detexify.kirelabs.org/classify.html
% https://www.overleaf.com/
% https://tex.stackexchange.com/
\end{document}
