document.addEventListener('DOMContentLoaded', function () {
  const checkbox = document.querySelector('.light-mode-checkbox');

  checkbox.checked = localStorage.getItem('lightMode') === 'true';

  checkbox.addEventListener('change', function (event) {
    localStorage.setItem('lightMode', event.currentTarget.checked);
  });
});
